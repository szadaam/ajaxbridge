package com.sztefanov.ajaxbridge.helper;

import com.sztefanov.ajaxbridge.helper.Files;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class FilesTest {

  public final static Logger LOGGER = LoggerFactory
    .getLogger(FilesTest.class);

  public FilesTest() {
  }

  /**
   * Test of fileToString method, of class Files.
   */
  @Test
  public void testFileToString() {
    System.out.println("fileToString");
    String filePath = "fileToStringTest.txt";
    String expResult = "Árvíztűrő tükörfúrógép\n"
      + "alma, banán, körte\n"
      + "";
    String result = null;
    try {
      result = Files.fileToString(filePath);
    } catch (IOException ex) {
      LOGGER.error("testFileToString.IOException", ex);
    }
    try {
      result = Files.fileToString(filePath);
    } catch (IOException ex) {
      LOGGER.error("testFileToString.IOException", ex);
    }
    assertEquals(expResult, result);
    // TODO review the generated test code and remove the default call to fail.
    //fail("The test case is a prototype.");
  }

  /**
   * Test of stringToFile method, of class Files.
   */
  @Test
  public void testStringToFile() throws Exception {
    System.out.println("stringToFile");
    String stringToFile = "";
    String targetFilePath = "";
    boolean appendToFile = false;
    Files.stringToFile(stringToFile, targetFilePath, appendToFile);
    // TODO review the generated test code and remove the default call to fail.
    fail("The test case is a prototype.");
  }

  /**
   * Test of scanDir method, of class Files.
   */
  @Test
  public void testScanDir() {
    System.out.println("scanDir");
    File dir = null;
    ArrayList<File> files = null;
    Files.scanDir(dir, files);
    // TODO review the generated test code and remove the default call to fail.
    fail("The test case is a prototype.");
  }

}
