package com.sztefanov.ajaxbridge.datastructure;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import org.json.JSONArray;
import org.json.JSONObject;
import org.json.JSONWriter;

public class JsonConverter {

  private JsonConverter() {
    // ...
  }

  public static Data stringToData(String jsonString) {
    Data data = new Data();
    JSONArray jsonArray = new JSONArray(jsonString);
    String[] jsonKeys = JSONObject.getNames(jsonString);
    for (int i = 0; i <= jsonKeys.length; i++) {
      String jsonKey = jsonKeys[i];
      String jsonValue = jsonArray.get(i).toString();
      data.put(jsonKey, jsonValue);
    }
    return data;
  }

  public static String dataToString(Data data) {
    return JSONWriter.valueToString(data);
  }

  public static String ObjectToString(Object obj) {
    return JSONWriter.valueToString(obj);
  }

  public static Object stringToObject(String jsonString) {
    return JSONObject.stringToValue(jsonString);

  }

  public static void jsonToFile(String jsonString, String filePath)
    throws IOException {
    Path path = Paths.get(filePath);
    Files.write(path, jsonString.getBytes(),
      StandardOpenOption.CREATE);
  }

}
