package com.sztefanov.ajaxbridge.datastructure;

import java.util.HashMap;

public class ObjectData extends HashMap<String, Object> {

  public String jsonSerialize() {
    return JsonConverter.ObjectToString(this);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    for (Entry<String, Object> entry : this.entrySet()) {
      String key = entry.getKey();
      String value = entry.getValue().toString();
      sb.append("\t[").append(key).append(" => ")
        .append(value).append("]\n");
    }
    return "ObjectData {\n" + sb.toString() + '}';
  }
}
